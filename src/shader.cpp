/**
	Runs a compute shader on the CPU (for debugging purposes)
**/
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <cstdio>

#define GLM_SWIZZLE
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace glm;

vec3 gl_WorkGroupID;

typedef void *sampler2D;

vec4 texture(const sampler2D tex, vec2 uv) {
	const int *preparams = (int*) tex;
	int w = *preparams, h = *(preparams+1);
	int offset = uv.t * h * w + uv.s * w;
	const float *data = (float*) (preparams+2);
	return vec4(data[offset]);
}

#define main glmain
#define layout(...)
#define buffer struct
#define uniform
#define in

// WARNING: left-value swizzles do not work in GLM (ie: vecxy.xy = pix.rg won't modify vecxy)
// TODO append parenthesis after swizzle operations, eg: vec3inst.xyz -> vec3inst.xyz()
// TODO replace array that have no length with pointers
// TODO remove #version ...
// TODO remove every #extension

/**
			BEGIN SHADER                                             # # # # #
**/
//#version 430 core
layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

// To translate light's coordinates to screen coordinates
uniform mat4 proj_m4;
uniform mat4 view_m4;

// Camera position in space coordinates, for light culling
uniform vec3 camera_pos_3d;

uniform vec2 screen_size;
uniform vec2   tile_size;
uniform int max_lights_per_tile;

// Depth texture made by the depth pre-pass
uniform sampler2D depth;

// Defines lights structures
struct PointLight {
	vec3 position;
	vec3 color;
	float intensity;
};

struct SpotLight {
	vec3 position;
	vec3 color;
	float intensity;
	vec3 direction;
	float angle;
	float penumbra_angle;
};

// Light SSBOs (input)
layout(std430, binding = 0) buffer _pointLights {
	uint count;
	PointLight *lights;
} point_lights;

layout(std430, binding = 1) buffer _spotLights {
	uint count;
	SpotLight *lights;
} spot_lights;

// Light indices SSBO (output)
layout(std430, binding = 2) buffer _indices {
	int *indices;
} indices;

// Computes the boundaries of the current tile, vec4 = (bl, ur)
// bl = vec2(x, y) = bottom left  corner coordinates
// ur = vec2(x, y) = upper  right corner coordinates
vec4 getTileBounds() {
	vec2 bl = gl_WorkGroupID.xy(); // WorkGroupID === TileID
	bl = bl * tile_size;
	vec2 ur = bl + tile_size;
	if (ur.x > screen_size.x) ur.x = screen_size.x;
	if (ur.y > screen_size.y) ur.y = screen_size.y;
	return vec4(bl, ur);
}

// Computes the depth frustum (Min and Max depth values) of the current tile
// Returns vec2(min, max)
vec2 getDepthFrustum(vec4 boundaries) {
	float i, j;
	float min = 1., max = 0., d;
	for (i=boundaries.x; i<boundaries.z; i++) {
		for (j=boundaries.y; j<boundaries.w; j++) {
			d = texture(depth, vec2(i/screen_size.x, j/screen_size.y)).r;
			if (d > max) max = d;
			if (d < min) min = d;
		}
	}
	return vec2(min, max);
}

// Returns the frustum of the current tile. mat3x2 = (xf, yf, zf)
// xf = vec2(x1, x2)  yf = vec2(y1, y2)  zf = vec2(z1, z2)  .1 <= .2
mat3x2 getTileFrustum() {
	vec4 tile_bounds = getTileBounds();
	vec2 tile_depth  = getDepthFrustum(tile_bounds);
	tile_bounds.x = tile_bounds.x / (screen_size.x / 2.) -1;
	tile_bounds.y = tile_bounds.y / (screen_size.y / 2.) -1;
	tile_bounds.z = tile_bounds.z / (screen_size.x / 2.) -1;
	tile_bounds.w = tile_bounds.w / (screen_size.y / 2.) -1;
	return mat3x2(tile_bounds.xz(), tile_bounds.yw(), tile_depth);
}

// GLSL 430 has a distance() function, but there is no built-in squareDistance function
float sqrDistance(vec4 a, vec4 b) {
	vec4 ab = b - a;
	return ab.x * ab.x + ab.y * ab.y + ab.z * ab.z;
}
float sqrDistance(vec3 a, vec3 b) { return sqrDistance(vec4(a, 1.), vec4(b, 1.)); }

// Returns the frustum of a pointlight's area of effect. mat3x2 = (xf, yf, zf)
// xf = vec2(x1, x2)  yf = vec2(y1, y2)  zf = vec2(z1, z2)  .1 <= .2
mat3x2 pointLightFrustum(int light_ind) {
	vec3 pos = point_lights.lights[light_ind].position;
	float intensity = point_lights.lights[light_ind].intensity;
	float att = intensity * 10. / sqrDistance(camera_pos_3d, pos); // FIXME if ==0
	float aspect_ratio = screen_size.x/((float)screen_size.y);
	vec4 eye = view_m4 * vec4(pos, 1.); // eye coordinates
	vec4 scrn = proj_m4 * vec4(0., 0., eye.z+intensity, 1.);
	vec4 scrf = proj_m4 * vec4(0., 0., eye.z-intensity, 1.);
	float zn = (scrn.z / scrn.w) * .5 + .5; // Z bounds of this light's frustum
	float zf = (scrf.z / scrf.w) * .5 + .5;
	vec4 clip = proj_m4 * eye; // clip coordinate
	pos = clip.xyz() / clip.w; // screen coordinates (normalized)
	return mat3x2(pos.x - att, pos.x + att, pos.y - (att * aspect_ratio), pos.y + (att * aspect_ratio), zn, zf);
}

int m_sign(float f) {
	return f >= 0.;
}

// Returns true if the given light overlaps with the given frustum.
bool overlaps(int light_ind, mat3x2 frustum) {
	mat3x2 light_frustum = pointLightFrustum(light_ind);
	float lx, ly, lz, tx, ty, tz; // x,y,z axis

	lx = frustum[0].y - light_frustum[0].x;
	tx = light_frustum[0].y - frustum[0].x;

	ly = frustum[1].y - light_frustum[1].x;
	ty = light_frustum[1].y - frustum[1].x;

	lz = frustum[2].y - light_frustum[2].x;
	tz = light_frustum[2].y - frustum[2].x;

	return m_sign(lx) == m_sign(tx) && m_sign(ly) == m_sign(ty) && m_sign(lz) == m_sign(tz);
}

// Adds the given light index to this tile's light list
// Returns false if the light index list is full, true otherwise
int tile_ind_offset = 0;
int current_index = 0;
bool addLight(int light_ind) {
	if (current_index == max_lights_per_tile) return false;
	indices.indices[tile_ind_offset + current_index] = light_ind;
	current_index++;
	return true;
}

// Marks the end of the `indices` SSBO with a '-1' (just like '\0' for a String)
void endOfArrayMark() {
	if (current_index != max_lights_per_tile) indices.indices[tile_ind_offset + current_index] = -1;
}

void main() {
	// Compute this tile's offset in output SSBO `indices`
	tile_ind_offset = int(gl_WorkGroupID.x * ceil(screen_size.y / tile_size.y) + gl_WorkGroupID.y) * max_lights_per_tile;

	// Frustum of the tile
	mat3x2 tile_frustum = getTileFrustum();

	// For all the lights
	for (unsigned int i=0; i<point_lights.count; i++) {
		// If it overlaps with the current tile
		if (overlaps(i, tile_frustum)) {
			// Adds it to this tile's list of lights
			if (!addLight(i)) break;
		}
	}

	endOfArrayMark();
}

/**
			END SHADER                                               # # # # #
**/

#undef main

#define PI  3.141592653589793
#define PI2 1.5707963267948966
#define PI3 1.0471975511965976
#define PI4 0.7853981633974483
#define PI5 0.6283185307179586
#define PI6 0.5235987755982989
#define PI7 0.4487989505128276
#define PI8 0.39269908169872414

vec3 cam_pos(15., 8., 15.);
vec3 cam_orient(-PI8, PI4, 0.);

int display();

/// Initializes uniforms and SSBOs, then call `glmain()`
int main(void) {
	proj_m4 = perspective(45., 800./600., 1., 100.);

	mat4 cam_rotX = rotate(mat4(1.), -cam_orient.x, vec3(1., 0., 0.));
	mat4 cam_rotY = rotate(cam_rotX, -cam_orient.y, vec3(0., 1., 0.));
	view_m4 = translate(cam_rotY, cam_pos * vec3(-1.));

	camera_pos_3d = cam_pos;
	screen_size = vec2(800, 600);
	tile_size = vec2(20, 20);
	max_lights_per_tile = 4;

	point_lights.count = 4;
	point_lights.lights = new PointLight[4];
	vec3 light_pos[4] = {
		vec3(-11, .25, -11),
		vec3(-11, .25,  11),
		vec3( 11, .25, -11),
		vec3( 11, .25,  11)
	};
	for (int i=0; i<4; i++) {
		point_lights.lights[i].color = vec3(.5, .5, .5);
		point_lights.lights[i].position = light_pos[i];
		point_lights.lights[i].intensity = 1.;
	}

	spot_lights.count = 0;
	spot_lights.lights = NULL;

	indices.indices = new int[30*40*max_lights_per_tile];

	depth = std::malloc(2*sizeof(int) + 800*600*sizeof(float));
	*((int*)depth) = 800;
	*(((int*)depth)+1) = 600;
	FILE *f = fopen("depth.float_array", "rb");
	fread((((int*)depth)+2), 1, 800*600*sizeof(float), f);
	fclose(f);

	for (int x=0; x<40; x++) {
		for (int y=0; y<30; y++) {
			current_index = 0;
			gl_WorkGroupID = vec3(x, y, 0);
			glmain();
		}
	}

	return display(); // return 0;
}

// ----------------

#undef layout
#undef buffer
#undef uniform
#undef in

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>

void draw_tile_density();
void draw_depth();
void draw_grid();
void draw_lights();

int win_tile_w, win_tile_h;

ALLEGRO_FONT *confont;

int display() {
	ALLEGRO_DISPLAY *dsp;
	if (!al_init()) return 1;

	al_set_app_name("DebugShader");
	al_set_new_display_flags(ALLEGRO_OPENGL);
	if (dsp = al_create_display(800, 600), !dsp) return 1;

	if (!al_install_keyboard()) return 1;
	if (!al_init_primitives_addon()) return 1;
	al_init_font_addon();

	confont = al_create_builtin_font();

	ALLEGRO_KEYBOARD_STATE state;
	while (al_get_keyboard_state(&state), !al_key_down(&state, ALLEGRO_KEY_ENTER)) {
		al_clear_to_color(al_map_rgb(30, 30, 30));
		draw_depth();
		draw_lights();
		draw_grid();
		draw_tile_density();
		al_flip_display();
		al_rest(.1);
	}

	al_destroy_display(dsp);
	al_shutdown_font_addon();

	return 0;
}

ALLEGRO_COLOR density_lut[8] = {
	{0., 0., 1., 1.},
	{0., .4, 1., 1.},
	{0., .7, 1., 1.},
	{0., 1., 1., 1.},
	{0., 1., 0., 1.},
	{1., 1., 0., 1.},
	{1., .5, 0., 1.},
	{1., 0., 0., 1.},
};

ALLEGRO_BITMAP* tile_density() {
	ALLEGRO_BITMAP *bmp, *prev;
	int x, y, i, tile_ind_offset, light, count, format, flags;

	flags = al_get_new_bitmap_flags();
	al_set_new_bitmap_flags(ALLEGRO_MEMORY_BITMAP);
	format = al_get_new_bitmap_format();
	al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_RGB_888);
	bmp = al_create_bitmap(win_tile_w, win_tile_h);
	al_set_new_bitmap_format(format);
	al_set_new_bitmap_flags(flags);
	prev = al_get_target_bitmap();
	al_set_target_bitmap(bmp);
	al_lock_bitmap(bmp, ALLEGRO_PIXEL_FORMAT_RGB_888, ALLEGRO_LOCK_WRITEONLY);

	for (x=0; x<win_tile_w; x++) {
		for (y=0; y<win_tile_h; y++) {
			count = 0;
			tile_ind_offset = (x * win_tile_h + y) * max_lights_per_tile;
			for (i=0; i<max_lights_per_tile; i++) {
				light = indices.indices[tile_ind_offset + i];
				if (light == -1) break;
				count++;
			}
			if (count >= 1)
				std::cout << "tile " << x << ',' << y << " = " << count << " lights" << std::endl;
			count = (count / (float)max_lights_per_tile) * 7;
			al_put_pixel(x, y, density_lut[count]);
		}
	}

	al_unlock_bitmap(bmp);
	al_set_target_bitmap(prev);
	return bmp;
}

void draw_tile_density() {
	static ALLEGRO_BITMAP *td = NULL;

	win_tile_w = screen_size.x / tile_size.x;
	win_tile_h = screen_size.y / tile_size.y;

	if (!td) td = tile_density();
	al_draw_scaled_bitmap(td, 0, 0, win_tile_w, win_tile_h, screen_size.x - (win_tile_w*4), screen_size.y - (win_tile_h*4), win_tile_w*4, win_tile_h*4, ALLEGRO_FLIP_VERTICAL);
}

void draw_depth() {
	static ALLEGRO_BITMAP *bmp = NULL;
	ALLEGRO_BITMAP *prev;
	int format;

	if (!bmp) {
		format = al_get_new_bitmap_format();
		al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_RGB_888);
		bmp = al_create_bitmap(screen_size.x, screen_size.y);
		al_set_new_bitmap_format(format);
		prev = al_get_target_bitmap();
		al_set_target_bitmap(bmp);
		al_lock_bitmap(bmp, ALLEGRO_PIXEL_FORMAT_RGB_888, ALLEGRO_LOCK_WRITEONLY);

		for (int x=0; x<screen_size.x; x++) {
			for (int y=0; y<screen_size.y; y++) {
				float z = texture(depth, vec2(x/screen_size.x, y/screen_size.y)).r, n = 1.0, f = 100.0;
				float col = (2.0 * n) / (f + n - z * (f - n));
				al_put_pixel(x, screen_size.y-y, al_map_rgb_f(col, col, col));
			}
		}

		al_unlock_bitmap(bmp);
		al_set_target_bitmap(prev);
	}

	al_draw_bitmap(bmp, 0, 0, 0);
}

void draw_lights() {
	for (unsigned int i=0; i<point_lights.count; i++) {
		mat3x2 lf = pointLightFrustum(i);
		vec2 pos(lf[0].x + (lf[0].y - lf[0].x)/2., lf[1].x + (lf[1].y - lf[1].x)/2.);
		vec2 r((lf[0].y - lf[0].x)/2., (lf[1].y - lf[1].x)/2.);

		al_draw_filled_ellipse(
			(pos.x+1.)/2. * screen_size.x, screen_size.y - ((pos.y+1.)/2. * screen_size.y),
			r.x/2. * screen_size.x, r.y/2. * screen_size.y, al_map_rgba_f(1., 0., 0., .2));

		al_draw_rectangle((lf[0].x + 1)/2. * screen_size.x, screen_size.y - (lf[1].x + 1)/2. * screen_size.y,
		                  (lf[0].y + 1)/2. * screen_size.x, screen_size.y - (lf[1].y + 1)/2. * screen_size.y,
		                  al_map_rgba_f(0., 1., 0., .2), 2.);
	}
}

void draw_grid() {
	int i, c;
	for (c=0, i=0; i<screen_size.x; c++, i+=tile_size.x) {
		al_draw_line(i, 0, i, screen_size.y, al_map_rgba_f(0., 0., 1., .2), 1.);
		al_draw_textf(confont, al_map_rgb(0, 0, 0), i+2, screen_size.y - tile_size.y + 2, 0, "%d", c);
	}

	for (c=0, i=screen_size.y; i>0; c++, i-=tile_size.y) {
		al_draw_line(0, i, screen_size.x, i, al_map_rgba_f(0., 0., 1., .2), 1.);
		al_draw_textf(confont, al_map_rgb(0, 0, 0), 2, i - tile_size.y + 2, 0, "%d", c);
	}
}
